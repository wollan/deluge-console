## Deluge console client

Connect to a deluge daemon from the console client.

#### Usage

Adding a torrent:

```
docker run --rm -it wollan/deluge-console
connect <server> <username> <password>
add -p /save/path https://...
info <torrent-hash>
exit
```

