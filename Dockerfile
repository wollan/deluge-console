FROM ubuntu:16.04
MAINTAINER Andreas Wohlén <andreas.wohlen@gmail.com>

RUN apt-get update \
 && apt-get install -y --no-install-recommends deluge-console \
 && rm -rf /var/lib/apt/lists/*

CMD ["deluge-console"]
